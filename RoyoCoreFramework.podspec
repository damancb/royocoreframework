Pod::Spec.new do |s|  
    s.name              = 'RoyoCoreFramework'
    s.version           = '0.0.5'
    s.summary           = 'The core framework includes Location manager, login with apple, Table view and collection view datasource classes, unary prefix for unwrapping option manager, Facebook login manager'
    s.homepage          = 'https://royoapps.com'

    s.author            = { 'Name' => 'damandeep@codebrewinnovations.com' }
    s.license           = { :type => 'MIT', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => "https://bitbucket.org/damancb/royocoreframework.git", :tag => "0.0.5" }

    s.ios.deployment_target = '12.0'
    s.ios.vendored_frameworks = 'RoyoCoreFramework.framework'

s.dependency 'SwiftyJSON'
s.dependency 'SkeletonView'
s.dependency 'AFNetworking'
s.dependency 'Alamofire'
s.dependency 'EZSwiftExtensions'
s.dependency 'NVActivityIndicatorView'
s.dependency 'ObjectMapper'
s.dependency 'FacebookCore'
s.dependency 'FacebookShare'
s.dependency 'FacebookLogin'
s.dependency 'ADCountryPicker'

end
